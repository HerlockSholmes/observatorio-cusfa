#+title: Taller Introductorio a la IA
#+author: Camilo Meza Gaete
#+options: toc:nil num:nil
#+reveal_root: https://cdn.jsdelivr.net/npm/reveal.js@5.0.2/
#+reveal_theme: beige
#+reveal_trans: linear
#+reveal_plugins: (highlight)

#+reveal: split
/Este libro está dedicado, con respeto y admiración al espíritu que vive en la
máquina/.

- Abelson, Harold, Gerald Jay Sussman, and Julie Sussman. Structure and
  Interpretation of Computer Programs. 2. ed., 7. [pr.]. Electrical Engineering
  and Computer Science Series. Cambridge, Mass.: MIT Press [u.a.], 2002.


* ¿Inteligencia?
