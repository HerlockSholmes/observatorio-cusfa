#+TITLE: Coordinación y regulación de la respuesta
#+TITLE: fisiológica Clase 4
#+AUTHOR: Camilo Meza Gaete
#+OPTIONS: toc:nil num:nil
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js@4.1.0/
#+REVEAL_THEME: moon
#+REVEAL_TRANS: linear

* Estructura Neuronal
** Potencial de reposo
En el estado de reposo, la neurona mantiene la diferencia de concentración de
iones entre el medio intra- y extracelular.

#+REVEAL: split
[[../../../imagenes/potencialDeReposo.png]]

#+REVEAL: split
Esta condición de reposo es un equilibrio dinámico: Las especies que entran y
las que salen no cambiarán las concentraciones, su *flujo neto es 0*.

#+REVEAL: split
[[../../../imagenes/concentracionesIntraExtraCelular.png]]

** Estímulos
Cuando una neurona recibe un estímulo, se abren los canales de \(Na^{+}\)
sensibles a voltaje.

Si el estímulo es lo suficientemente intenso, ocurrirá un proceso de
/despolarización/ de la neurona. Si el estímulo no es lo suficientemente
intenso, no se alcanza este proceso de despolarización. A esto se le denomina
*la ley del todo o nada*.

#+REVEAL: split
[[../../../imagenes/estimuloNervioso1.png]]

#+REVEAL: split
Inmediatamente al alcanzar un potencial de membrana superior a los +30 [mV], se
abrirán canales de potasio dependientes de voltaje, lo que permitirá a la neurona
volver a su potencial de reposo.

A este proceso se le denomina /repolarización/.

#+REVEAL: split
[[../../../imagenes/estimuloNervioso2.png]]

#+REVEAL: split
[[../../../imagenes/potencialDeAccion.png]]

- A :: Fase de despolarización, se abren los canales de sodio sensibles a
  voltaje, y este catión ingresa a la célula.

#+REVEAL: split
- B :: Fase de repolarización, se cierran los canales de sodio sensibles a
  voltaje y se abren los de potasio, saliendo este último catión de forma
  masiva hacia el medio extracelular.

#+REVEAL: split
- C :: Fase de hiperpolarización, la salida de cationes potasio es tal que la
  neurona queda con una diferencia de aniones mucho mayor al estado de reposo.

#+REVEAL: split
- D :: Vuelta al estado de reposo, gracias a la acción de la bomba de \(Na^{+}/K^{+}\)
  ATPasa.

** Pregunta tipo prueba
En la siguiente tabla se hacen asociaciones entre un término y su definición.
¿Cuál es la única correcta?

| Término             | Definición                                                     |
|---------------------+----------------------------------------------------------------|
| Despolarización     | Salida masiva de iones \(K^{+}\) que provoca                   |
|                     | que el interior celular se torne más negativo de lo normal.    |
| Potencial de acción | Aumento de la permeabilidad de la membrana para los            |
|                     | iones de \(Na{+}\), lo que provoca que el interior celular     |
|                     | se vuelva positivo respecto al exterior.                       |

#+REVEAL: split
| Repolarización      | Cambio transitorio en el potencial eléctrico de la membrana    |
|                     | celular, que permite la conducción del impulso nervioso.       |
| Potencial de reposo | La carga eléctrica es positiva en el medio externo e           |
|                     | interno de la membrana, puesto que no hay movimiento de iones. |
| Hiperpolarización   | Aumento en la permeabilidad de la membrana para el \(Na{+}\),  |
|                     | lo que provoca su salida masiva.                               |


* Potencial de acción
** Características
- Se propaga en una sola dirección (/unidireccional/) a lo largo del axón.
- Responde a la ley del todo o nada, lo que significa que sólo se activa con los
  estímulos que sean suficientemente intensos.
- Es continuo (en ausencia de vainas de mielina) o saltatorio (en presencia de
  vainas de mielina).

** Factores que afectan
- Diámetro del axón :: Mientras mayor sea el diámetro del axón, mayor superficie de
  contacto y por lo tanto mayor será la rapidez de propagación del potencial de acción.
- Vainas de mielina :: Al ser saltatorio, la velocidad de propagación del potencial de
  acción aumenta.

#+REVEAL: split
[[../../../imagenes/comparacionMielinizadaDesmielinizada.png]]

** Pregunta tipo
¿Cuál de los siguientes axones conduce más rápidamente el impulso nervioso?

[[../../../imagenes/preguntaMielinizada.png]]

* Sinapsis y neurotransmisores
** Sinapsis
Corresponde a la comunicación entre dos neuronas, permitiendo que el potencial de acción
de una neurona previa continúe o se detenga en la siguiente. Es el fenómeno funcional
del sistema nervioso.

** Sinapsis eléctrica
La comunicación ocurre por el paso directo del potencial de acción de una neurona a otra.

[[../../../imagenes/sinapsisElectrica.png]]

** Sinapsis química
En el botón sináptico se almacenan vesículas con neurotransmisores, una vez llega el potencial
de acción al botón, ocurren los siguientes cambios:

[[../../../imagenes/sinapsisQuimica1.png]]

#+REVEAL: split
[[../../../imagenes/sinapsisQuimica2.png]]

** Excitación
Las sinapsis químicas pueden excitar la neurona postsináptica, provocando la propagación de un
potencial de acción en ella.

#+REVEAL: split
[[../../../imagenes/sinapsisExcitatoria.png]]


** Inhibición
También se puede dar el caso de que el neurotransmisor termine por hiperpolarizar la neurona
postsináptica, por lo que está evitando que se propague el potencial de acción.

#+REVEAL: split
[[../../../imagenes/sinapsisInhibitoria.png]]

** Neurotransmisores
Son las sustancias que libera la neurona presináptica en la sinapsis química. Podemos clasificarlos
en las siguientes tres categorías según su naturaleza química:

#+REVEAL: split
+ Aminoácidos :: Son los neurotransmisores que tienen funciones excitatorias o inhibitorias. Por ejemplo
  el GABA, Glutamato, Glicina.
+ Aminas biogénicas :: Son los neurotransmisores relacionados con el comportamiento emocional y la recompensa.
  Por ejemplo la Dopamina, Catecolaminas, Indolamina.
+ Acetilcolina :: Actúa principalmente en la unión neuromuscular.
