#+TITLE: Cosmos Clase 1
#+AUTHOR: Camilo Meza Gaete
#+OPTIONS: toc:nil num:nil
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js@4.1.0/
#+REVEAL_THEME: moon
#+REVEAL_TRANS: linear

* Aristóteles y Ptolomeo
** El mundo antiguo
[[../../imagenes/FrescoPlatonAristoteles.jpg]]

* Giordano Bruno

* Nicolás Copérnico

* Galileo Galilei

* Un paréntesis filosófico

** Empirismo

** Falsacionismo

** Paradigmas
