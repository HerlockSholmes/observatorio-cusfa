#+TITLE: Fuerza y Aceleración Clase 1
#+author: Camilo Meza Gaete
#+OPTIONS: toc:nil num:nil
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js@4.1.0/
#+REVEAL_THEME: solarized
#+REVEAL_TRANS: linear

* Aceleración
** Aceleración media
Cuando se realiza un cambio de velocidad el movimiento deja de ser uniforme y
comienza a denominarse Movimiento Rectilíneo Uniformemente Variado (MRUV).

#+reveal: split
La *aceleración media* corresponde a una magnitud vectorial que se puede definir
como el cambio de velocidad por unidad de tiempo, su ecuación es:

\[\vec a = \frac{\delta \vec \nu}{\delta t} \]

Su unidad en el S.I. es \(\left [ \frac{m}{s^{2}} \right ]\).

#+reveal: split
Pero por simplicidad y falta de herramientas matemáticas a este nivel académico
utilizaremos su simplificación:

\[\vec a = \frac{\vec \nu_{f} - \vec \nu_{i}}{t} \]

Donde \(\vec \nu_{f}\) y \(\vec \nu_{i}\) son la velocidad final e inicial,
respectivamente.

#+reveal: split
La aceleración se puede dar debido a un cambio en la dirección del movimiento:

[[../../../imagenes/cambioDireccion.png]]

#+reveal: split
También, se puede dar debido a un cambio de sentido:

[[../../../imagenes/cambioSentido.png]]

** Movimientos con aceleración constante
Si un cuerpo mantiene una aceleración constante, su velocidad va aumentando o
disminuyendo de manera ordenada en el tiempo mientras se desplaza en trayectoria
rectilínea.

#+reveal: split
Así, podemos plantear la siguiente expresión para conocer la rapidez de un
cuerpo con aceleración constante:

\[\nu_{f} = \nu_{i} + a \cdot t\]

** Representación gráfica
Podemos representar gráficamente la aceleración en el tiempo, como es constante
tendrá la siguiente forma:

#+reveal: split
[[../../../imagenes/graficoAceleracion.png]]

#+reveal: split
Si calculamos el área bajo la curva, obtendremos el cambio de velocidad que
experimenta el cuerpo.

#+reveal: split
Si estudiamos un gráfico de la velocidad en función del tiempo para
aceleraciones positivas tendremos:

#+reveal: split
[[../../../imagenes/graficoVelocidad1.png]]

#+reveal: split
Al calcular el área bajo la curva de un gráfico velocidad v/s tiempo,
obtendremos la distancia recorrida por el móvil.

#+reveal: split
Si el trabajo se nos complica, podemos proyectar figuras geométricas conocidas
(triángulos, cuadrados, o rectángulos).

#+reveal: split
[[../../../imagenes/graficoVelocidad2.png]]

** Posición en función del tiempo
Podemos determinar la posición de un cuerpo en cualquier instante de tiempo si
parte desde una posición \(r_{i}\), con una velocidad \(\vec \nu_{i}\), y con una
aceleración constante \(\vec a\).

#+reveal: split
\[\vec r_{f} = \vec r_{i} + \vec \nu_{i} \cdot t + \frac{1}{2} \cdot \vec a \cdot t^{2} \]

Recordando que los valores de esos escalares pueden ser positivos o negativos
dependiendo del sistema de referencia.
