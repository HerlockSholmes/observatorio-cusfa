#+TITLE: Luz y óptica geométrica Clase2
#+author: Camilo Meza Gaete
#+OPTIONS: toc:nil num:nil
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js@4.1.0/
#+REVEAL_THEME: beige
#+REVEAL_TRANS: linear

* Propagación de la luz
** Velocidad de la luz
Cuando viaja por el vacío, la luz se propaga a una velocidad de
\(300.000 \left [ \frac{km}{s} \right ]\)

Esta velocidad es una constante representada por la letra \(c\) y corresponde a
la mayor rapidez teórica que puede alcanzar un cuerpo.

#+reveal: split
Cuando se propaga por otro medio, su rapidez depende de las características de
dicho medio.

Cada medio tendrá su propio índice de refracción dependiendo del cambio de
velocidad que sufre la luz.

#+reveal: split
| Medio    | Velocidad [km/s] | Índice de refracción |
|----------+------------------+----------------------|
| Vacío    |          300.000 | 1                    |
| Aire     |          300.000 | 1                    |
| Agua     |          225.000 | 1,33                 |
| Diamante |          124.000 | 2,4                  |

** Aplicaciones del índice de refracción
Cuando la luz viaja por un medio material, cada frecuencia viaja a una velocidad
ligeramente diferente lo que provoca efectos como los siguientes:

[[../../../imagenes/reflexionTotalInterna.png]]

#+reveal: split
Este fenómeno de reflexión total interna es lo que da funcionamiento a la
tecnología conocida como *fibra óptica*.

* Dispersión de la luz
** Descomposición de la luz visible
La luz blanca está compuesta por una superposición de luces de distinto color.
Cada uno de estos colores corresponde a una onda de luz del espectro visible con
una frecuencia determinada, la que es distinta para cada color.

[[../../../imagenes/prisma.gif]]

#+reveal: split
Por ejemplo, la formación de un arcoiris se debe a este fenómeno:

[[../../../imagenes/arcoiris.png]]

* Luz láser
** Incoherencia de la luz blanca
La luz blanca es incoherente, es decir, está formada por ondas de distinta
frecuencia que se encuentran fuera de fase  (atrasadas o adelantadas unas
respecto de otras).

[[../../../imagenes/luzBlanca.png]]

** Luz monocromática
La luz monocromática (de un solo color) está formada por ondas de una misma
frecuencia, pero que también están fuera de fase. Por ejemplo, la luz emitida
por un led.

[[../../../imagenes/luzLed.png]]

** Luz láser
Si la luz está formada por ondas de la misma frecuencia, que viajan todas en
fase, se dice que es coherente. Este tipo de luz es llamada luz LÁSER.

[[../../../imagenes/luzLaser.png]]
