#+TITLE: Medioambiente y sostenibilidad Clase 3
#+author: Camilo Meza Gaete
#+OPTIONS: toc:nil num:nil
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js@4.1.0/
#+REVEAL_THEME: beige
#+REVEAL_TRANS: linear

* La naturaleza también tiene derechos
** Conflictos medioambientales
Como ya hemos estudiado, la actividad humana tiene un impacto sobre el entorno.

#+reveal: split
Y es posible que este impacto también incurra en una vulneración de los Derechos
Humanos, por esto el Instituto Nacional de Derechos Humanos hizo la siguiente
investigación:

#+reveal: split
Se tomó conocimiento de todos los eventos de contaminación o destrucción de los
ambientes por parte de particulares o a consecuencia del actuar estatal, al 2018
los conflictos eran los siguientes:

#+reveal: split
[[../../../imagenes/conflictosMedioambientales.png]]

#+reveal: split
De hacer un mapa, nos encontramos con:

[[../../../imagenes/mapaConflictosSocioambientalesChile.png]]

** Aterricemos a una realidad
Existen lugares cuyos conflictos socioambientales están activos o han sido
ignorados.

Estos lugares se conocen como "*Zonas de sacrificio*" por sus bajos índices de
calidad de vida.

** Leamos una noticia
Analiza la noticia que se te entrega, considerando las preguntas básicas del
¿Qué?, ¿Cómo?, ¿Cuándo?, ¿Dónde?, así como sus potenciales causas.

¿Tienen solución estos conflictos?.

** Derechos consagrados en leyes o constituciones
