#+TITLE: Consumo Sostenible y protección ambiental Clase 1
#+author: Camilo Meza Gaete
#+options: toc:nil num:nil
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js@4.1.0/
#+REVEAL_THEME: white
#+REVEAL_TRANS: linear

* ¿Cómo impacta la economía al medio ambiente?
Piensa en un plato de comida: ¿Conoces la receta? ¿Cuáles son sus ingredientes?
¿Cuánto tiempo toma el producirlos?.

#+reveal: split
[[../../../imagenes/pieDeManzana.jpg]]

** Concepto de materia prima
A todos los constituyentes básicos de un producto, se les denomina /materia/
/prima/.

La economía funciona a través de los productos o servicios que se hacen
trabajando sobre estos materiales.

** Investiga la producción de
+ Los ingredientes de tu comida favorita.
+ Un teléfono móvil.
+ Una red social que utilices.

* De la cuna a la tumba
En todos esos casos, se puede establecer una línea que inicia en las materias
primas y termina en los desechos.

** Leyes químicas
+ "La materia no se crea ni se destruye, solo se transforma" (ley fundamental de
  la química).
+ "La energía no se puede crear ni destruir, solo transferirse mediante
  transformaciones" (primera ley de la termodinámica).

#+reveal: split
Siguiendo esas leyes, el camino de producción lineal que tiene la economía nos
lleva inevitablemente a la producción de residuos que no se pueden reintegrar.

* ¿Qué hacer?
Analicemos el caso de la producción de cerámicos y baldosas, estudiando su
consumo de agua y energía.

Trabajo obtenido de:
Benveniste et al., “Análisis de Ciclo de Vida y Reglas de
Categoría de Producto En La Construcción. El Caso de Las Baldosas
Cerámicas,” 75.

#+reveal: split
[[../../../imagenes/entradasCeramicosBenveniste.png]]

#+reveal: split
[[../../../imagenes/salidasCeramicosBenveniste.png]]

** ¿Qué podemos hacer desde nuestra realidad local?
Hemos visto que la economía tiene un impacto en la generación de residuos, desde
nuestra comuna ¿Podemos aportar aminorando este impacto? ¿Qué actividades y/o
empresas son las que más afectan el medioambiente dentro de nuestra provincia?.
